FROM golang AS build

ADD . /go/src/gitlab.com/k8s-torrent/k8s-torrent-collector
ADD gitconfig /root/.gitconfig


# Install and patch torrent library
WORKDIR /go
RUN go get github.com/anacrolix/torrent
WORKDIR /go/src/github.com/anacrolix/torrent 
RUN git am < /go/src/gitlab.com/k8s-torrent/k8s-torrent-collector/0001-Hack-to-get-k8s-torrent-working.patch 

# Build app
WORKDIR /go/src/gitlab.com/k8s-torrent/k8s-torrent-collector
RUN go get
# RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build main.go
RUN go build -ldflags "-linkmode external -extldflags -static" -a main.go

# # Copy build to lightweight image
FROM scratch
COPY --from=build /go/src/gitlab.com/k8s-torrent/k8s-torrent-collector/main /main
