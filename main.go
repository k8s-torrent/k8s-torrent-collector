// TODO
// Test Minio connection on startup
// Shutdown after storing the downloaded torrent

package main

import (
	// "bytes"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/anacrolix/torrent"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/minio/minio-go/v6"
	"html/template"
	"io"
	"io/ioutil"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"log"
	"net/http"
	"os"
	"strings"
)

type Args struct {
	Kubeconfig  string
	Torrentfile string
	Outputdir   string
	MinioConfig MinioArgs
}

type MinioArgs struct {
	Endpoint        string
	AccessKeyID     string
	SecretAccessKey string
	Bucketname      string
}

type Torrentfile struct {
	gorm.Model
	Location     string
	IsDownloaded bool
}

type Page struct {
	Title       string
	Body        []byte
	Torrentfile Torrentfile
}

var db *gorm.DB
var client *torrent.Client
var pieceCount int
var completed bool
var args Args

func main() {
	var err error

	args = ParseArgs()
	fmt.Printf("Args: %v\n", args)

	CreateDirIfNotExist(args.Outputdir)

	// kubeconfig
	config, _ := clientcmd.BuildConfigFromFlags("", args.Kubeconfig)
	// creates the clientset
	clientset, _ := kubernetes.NewForConfig(config)
	// access the API to list pods
	pods, _ := clientset.CoreV1().Pods("").List(v1.ListOptions{})
	fmt.Printf("There are %d pods in the cluster\n", len(pods.Items))

	db, err = gorm.Open("sqlite3", "torrentfile.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()
	db.LogMode(true)

	// Migrate the schema
	db.AutoMigrate(&Torrentfile{})

	// Load torrentfile into db
	torrentfile := &Torrentfile{Location: args.Torrentfile}
	fmt.Printf("DB: %v\n", torrentfile)
	if db.First(&torrentfile, "location = ?", torrentfile.Location).RecordNotFound() {
		log.Print("Record not found, creating ...")
		db.Create(torrentfile)
	}
	fmt.Printf("DB: %v\n", torrentfile)

	// 1 Start webserver
	r := mux.NewRouter()
	r.HandleFunc("/", torrentfile.rootHandler).Methods("GET")
	r.HandleFunc("/torrentfile", torrentfile.torrentfileGetHandler).Methods("GET")
	r.HandleFunc("/torrentfile/download", torrentfile.torrentfileDownloadHandler).Methods("GET")
	r.HandleFunc("/torrentfile", torrentfile.torrentfilePostHandler).Methods("POST")
	r.HandleFunc("/pieces/{id:[0-9]+}", torrentfile.piecePostHandler).Methods("POST")
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(":8080", nil))
	// 2 Schedule TorrentDownloader if it is not downloaded yet
	// 3	Store torrent info
}

func ParseArgs() Args {
	kubeconfigOption := flag.String("k", "", "Kubeconfig to be used")
	torrentfileOption := flag.String("t", "", "Torrentfile to download")
	outputdirOption := flag.String("o", "/tmp/output", "Outputdir")
	minioEndpointOption := flag.String("me", "192.168.11.22:9000", "Minio Endpoint")
	minioAccessKeyIDOption := flag.String("mid", "AKIAIOSWUTNN7EXAMPLE", "Minio Access Key ID")
	minioSecretAccessKeyOption := flag.String("msecret", "wJalrXUtnFEMIK7WAATGbPxRfiCYEXAMPLEKEY", "Minio Secret Access Key")
	minioBucketnameOption := flag.String("mbucket", "media", "Minio Bucketname")

	flag.Parse()

	if *torrentfileOption == "" {
		flag.Usage()
		os.Exit(1)
	}

	return Args{*kubeconfigOption, *torrentfileOption, *outputdirOption, MinioArgs{*minioEndpointOption, *minioAccessKeyIDOption, *minioSecretAccessKeyOption, *minioBucketnameOption}}
}

func (torrentfile Torrentfile) rootHandler(w http.ResponseWriter, r *http.Request) {
	p, err := loadPage("root")
	if err != nil {
		p = &Page{Title: "Torrent Collector", Torrentfile: torrentfile}
	}
	t, _ := template.ParseFiles("html/root.html")
	t.Execute(w, p)
}

func (torrentfile Torrentfile) torrentfileGetHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("GET /torrentfile")
	js, err := json.Marshal(torrentfile)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func (torrentfile Torrentfile) torrentfileDownloadHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("GET /torrentfile/download")
	http.ServeFile(w, r, "./torrentfile")
}

func (torrentfile *Torrentfile) torrentfilePostHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("POST /torrentfile")

	// var Buf bytes.Buffer
	file, header, err := r.FormFile("torrentfile")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	f, err := os.OpenFile("./torrentfile", os.O_WRONLY|os.O_CREATE, 0666)
	defer f.Close()
	io.Copy(f, file)
	name := strings.Split(header.Filename, ".")
	log.Print("Torrentfile uploaded ", name)

	// Update torrentfile record
	// torrentfile.IsDownloaded = true
	// log.Print("Torrentfile updating ", torrentfile)
	// if err := db.Save(&torrentfile).Error; err != nil {
	// 	log.Print("Torrentfile could not be updated ", err)
	// }

	// Start torrent client
	client, err := torrent.NewClient(nil)
	fmt.Printf("Client %v\n", client)
	defer client.Close()
	t, err := client.AddTorrentFromFile("torrentfile")
	if err != nil {
		log.Print("Could not load torrentfile", err)
	}
	fmt.Printf("Torrent %v\n", t)
	<-t.GotInfo()
	pieceCount = t.NumPieces()
	fmt.Printf("Info : %v", t.Info().Name)
	fmt.Printf("Files : %v", len(t.Files()))
	fmt.Printf("File : %v", t.Files()[0].Path())
	for i := 0; i < len(t.Files()); i++ {
		// Read filename
		fmt.Printf("File : %v", t.Files()[i].Path())
	}

	fmt.Printf("-----------------------------------------------------------------------\n")
	fmt.Printf("%v pieces of %v bytes\n", t.NumPieces(), t.Info().PieceLength)
	fmt.Printf("-----------------------------------------------------------------------\n")

	return
}

func finalize(dir string) {
	client, err := torrent.NewClient(nil)
	fmt.Printf("Client %v\n", client)
	defer client.Close()
	t, err := client.AddTorrentFromFile("torrentfile")
	if err != nil {
		log.Print("Could not load torrentfile", err)
	}
	fmt.Printf("Torrent %v\n", t)
	<-t.GotInfo()
	fmt.Printf("Merging files from dir %v\n", dir)
	t.MakePeaces()
	// ip := p.Info(i)
	for i := 0; i < t.NumPieces(); i++ {
		filepath := fmt.Sprintf("%v/%v", dir, i)
		p := t.Peace(i)
		fmt.Printf("PI: %v\n", p)

		data, err := ioutil.ReadFile(filepath)
		if err != nil {
			fmt.Println("File reading error\n", err)
			os.Exit(1)
		}

		n, err := p.Storage().WriteAt(data, 0)
		if err != nil {
			fmt.Printf("Piece #%v Write error\n", i, err)
			os.Exit(1)
		}
		fmt.Printf("WriteAt #%v:\t%v\n", i, n)
	}

	fmt.Printf("Finished")
	completed = true

	// Upload to media storage
	UploadToMediaStorage(t)
	os.Exit(0)
}

func (torrentfile *Torrentfile) piecePostHandler(w http.ResponseWriter, r *http.Request) {
	if completed {
		log.Print("Torrent completed, not accepting any more uploads")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Torrent completed, not accepting any more uploads"))
	} else {
		vars := mux.Vars(r)
		id := vars["id"]
		log.Print("POST /pieces/", id)
		file, header, err := r.FormFile("piece")
		if err != nil {
			panic(err)
		}
		defer file.Close()
		filename := fmt.Sprintf("%v/%v", args.Outputdir, id)
		fmt.Printf("filename %v", filename)
		f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, 0666)
		defer f.Close()
		io.Copy(f, file)
		name := strings.Split(header.Filename, ".")
		log.Print("Torrentfile uploaded ", name)
		// Check if all pieces have been uploaded
		files, _ := ioutil.ReadDir(args.Outputdir)
		if len(files) == pieceCount {
			log.Print("All pieces have been uploaded")
			// Finalize the file
			finalize(args.Outputdir)
		} else {
			log.Print("PieceCount ", len(files))
		}
	}
}

func loadPage(title string) (*Page, error) {
	filename := "html/" + title + "..html"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	} else {
		return &Page{Title: title, Body: body}, nil
	}
}

func CreateDirIfNotExist(dir string) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			panic(err)
		}
	}
}

func UploadToMediaStorage(t *torrent.Torrent) {
	fmt.Printf("\nUploading...\n")
	useSSL := false
	minioClient, err := minio.New(args.MinioConfig.Endpoint, args.MinioConfig.AccessKeyID, args.MinioConfig.SecretAccessKey, useSSL)
	if err != nil {
		log.Fatalln(err)
	}
	for i := 0; i < len(t.Files()); i++ {
		file := t.Files()[i]
		// Read filename
		fmt.Printf("File : %v", t.Files()[i].Path())
		objectName := file.Path()
		filePath := file.Path()
		contentType := "application/zip"
		// Upload the zip file with FPutObject
		n, err := minioClient.FPutObject(args.MinioConfig.Bucketname, objectName, filePath, minio.PutObjectOptions{ContentType: contentType})
		if err != nil {
			log.Fatalln(err)
		}
		log.Printf("Successfully uploaded %s of size %d\n", objectName, n)
	}
}
