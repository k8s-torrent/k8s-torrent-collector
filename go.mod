module gitlab.com/tmestdagh/k8s-torrent-collector

go 1.13

require (
	github.com/anacrolix/torrent v1.9.0
	github.com/gorilla/mux v1.6.2
	github.com/jinzhu/gorm v1.9.11
	github.com/minio/minio-go/v6 v6.0.44
	github.com/spaolacci/murmur3 v1.1.0 // indirect
	k8s.io/apimachinery v0.0.0-20191115015347-3c7067801da2
	k8s.io/client-go v0.0.0-20191115215802-0a8a1d7b7fae
)

replace github.com/anacrolix/torrent => ../../../github.com/anacrolix/torrent
